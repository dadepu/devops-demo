## Stage 1: Maven Build
#FROM maven:3.9.1-eclipse-temurin-17-alpine as builder
#
## Set working directory
#WORKDIR /app
#
## Copy pom.xml and source code to the container
#COPY pom.xml .
#
## Load and cache dependencies
#RUN mvn dependency:go-offline
#
## Copy source code
#COPY src ./src
#
## Run Maven build
#RUN mvn -B -e clean package -DskipTests

# Stage 2: Build final image
FROM eclipse-temurin:20-jre-alpine

## Set working directory
WORKDIR /app

# Copy built JAR file from the builder stage
#COPY /app/target/*.jar ./app.jar
COPY /target/*.jar ./app.jar

# Expose application port
EXPOSE 8080

# Start the application
CMD ["java", "-jar", "app.jar"]
